# WTF is this shit ?

This project is a game made during a student game jam at our school.

The subject was **Act first, think never** so we decided to build a game where you should never think and instead execute orders. Stupidity is a major component of the game, just like speed.

You can download compiled versions or do it by yourself with Unity 5.4.6.