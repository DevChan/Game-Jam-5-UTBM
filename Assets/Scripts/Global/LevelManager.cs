﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public string[] levels;

	private string TirerAuSort(string ancienTirage){
		string tirage;

		do {
			tirage = levels[(int) (Random.Range(0, levels.Length))];
		} while (tirage == ancienTirage);

		return tirage;
	}

	public string ChargerScene(string actuel){
		string NouvelleScene;
		
		NouvelleScene = TirerAuSort(actuel);
		SceneManager.LoadScene(NouvelleScene);

		return NouvelleScene;
	}

	public void Mourir(){
		SceneManager.LoadScene("Game Over");
	}

	public void MenuPrincipal(){
		SceneManager.LoadScene("Menu");
	}

	public void Quitter(){
		Application.Quit();
	}

}
