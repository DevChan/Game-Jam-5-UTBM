﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {
	public Slider Chrono;
	private float startedTime,finishTime;
	private float Delay ;

	// Use this for initialization
	void Start () {
		Chrono.interactable = false;
		ResetTimer();

	}
	
	// Update is called once per frame
	void Update () {
		Chrono.value = ((finishTime - Time.time) / Delay);
		GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager> ().isEnded = (Chrono.value == 0);

	}
	public void NextTimer()
	{
		startedTime = Time.time;
		Delay *= 0.9f;
		finishTime = startedTime + Delay;
	}
	public void ResetTimer()
	{
		startedTime = Time.time;
		Delay = 30;
		finishTime = startedTime + Delay;

	}

}
