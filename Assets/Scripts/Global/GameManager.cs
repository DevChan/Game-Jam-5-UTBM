﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{

	//Script de singleton persistent
	private static GameManager _instance;

	public static GameManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<GameManager>();

				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}

			return _instance;
		}
	}

	void Awake() 
	{
		DontDestroyOnLoad (this);
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}

	// Ici on édite

	public int Score = 0 ;
	public const string BESTSCORE_KEY = "BestScore";
	private string scene_actuelle = "Menu";
	public bool isEnded;
	public AudioSource Music;
	public AudioSource Effects;

	public bool playerConnected = false;

	public void Perdu(){
//		Debug.Log ("perdu");
		Timer timer = this.gameObject.GetComponent<Timer>();
		timer.ResetTimer();
		Effects.Play();
		LevelManager lmanager = this.gameObject.GetComponent<LevelManager>();
		lmanager.Mourir();
	}
	public void Recommencer(){
		Score = 0;
		Timer timer = this.gameObject.GetComponent<Timer>();
		timer.ResetTimer();
		Music.pitch = 1;
		LevelManager lmanager = this.gameObject.GetComponent<LevelManager>();
		lmanager.MenuPrincipal();
	}

	public void Gagne(){
//		Debug.Log ("gagné");
		Timer timer = this.gameObject.GetComponent<Timer>();
		timer.NextTimer();
		Score++;
		if (Music.pitch < 3) {
			Music.pitch *= 1.05f;
		}
		LevelManager lmanager = this.gameObject.GetComponent<LevelManager>();
		scene_actuelle = lmanager.ChargerScene(scene_actuelle);

	}
	public void Exit(){
		LevelManager lmanager = this.gameObject.GetComponent<LevelManager>();
		lmanager.Quitter();
	}

	public bool sendScore(){
		if (playerConnected) {
			try{
				FindObjectOfType<XtralifeManager> ().PostScore (Score);
			}catch{
				Debug.Log ("XtraLife Coroutine's thing not working");
			}
		}
		if (PlayerPrefs.HasKey (BESTSCORE_KEY)) {
			if (Score > PlayerPrefs.GetInt (BESTSCORE_KEY)) {
				PlayerPrefs.SetInt (BESTSCORE_KEY, Score); 
				return true; //new Best
			} else {
				return false; //not new best
			}
		} else {
			PlayerPrefs.SetInt (BESTSCORE_KEY,Score);
			return true; //new Best
		}

	}
	public void SetScoreboard(Text board){
		if (playerConnected) {
			XtralifeManager xtra = FindObjectOfType<XtralifeManager> ();
			xtra.GetRank (PlayerPrefs.GetInt (BESTSCORE_KEY),board);
			xtra.CenteredScores (board);
		}
	}

	public string getUsername(){
		XtralifeManager xtra = FindObjectOfType<XtralifeManager> ();
		return xtra.getGamer ().GamerId.Substring (0, 7);
	}
}