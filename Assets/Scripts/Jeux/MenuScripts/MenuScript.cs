﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {
	public string esc="esc";
	//private const string reinit="change user";
	private int inReinit = 0;
	private int i=0;
	private GameManager Manager;

	public Text user;
	// Use this for initialization
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();

	
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape) || Manager.isEnded) {
			Perdu ();
		}
		foreach (char c in Input.inputString.ToLower()) {
			if (i < 3) {
				if (esc [i] == c) {
					i++;
					if (i == 3) {
						Manager.Gagne ();
					}
				} else {
					i = 0;
					//Debug.Log ("fail");
				}
			} 
		}
		/*foreach (char c in Input.inputString.ToLower()) {
			if (reinit [inReinit] == c) {
				inReinit++;
				if (inReinit == reinit.Length) {
					FindObjectOfType<XtralifeManager> ().newLogin ();
					inReinit = 0;
				}
			} else {
				inReinit = 0;
			} 
		}*/
		if (Manager.playerConnected) {
			user.text = "user : " + Manager.getUsername();
		}
	}
	public void Perdu(){
		Manager.Perdu ();
	}
}
