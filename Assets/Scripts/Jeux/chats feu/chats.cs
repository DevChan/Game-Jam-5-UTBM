using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using UnityEditor;

public class chats : MonoBehaviour {

	public int catLimit;
	public int killedCats;
	public GameObject catPrefab;
	public GameObject firePrefab;
	public AudioClip[] Meows;

	private int numberOfCats;
	private GameManager manager;

	// Use this for initialization
	void Start () {
		Vector3 firePosition;

		Text newOrder;

		killedCats = 0;

		manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

		numberOfCats = Random.Range(0, catLimit);

		newOrder = GameObject.FindGameObjectWithTag("Order").GetComponent<Text>();
		newOrder.text = "Throw " + numberOfCats + " cat(s) in the fire";

		firePosition = GenVect();
		SpawnPrefabWithPosition(firePrefab, firePosition);

		SpawnCats();

	}

	void Update (){
		if (manager.isEnded){
			manager.Perdu();
		}
	}

	void SpawnCats(){

		Vector3 screenDimension;

		int i;

		for(i=0; i < catLimit; i++){
			do {
				killedCats = 0;
				screenDimension = GenVect();
				SpawnPrefabWithPosition(catPrefab, screenDimension);

			} while (killedCats > 0);
		}
	}

	Vector3 GenVect(){

		Vector3 screenDimension;

		screenDimension.x = Screen.width;
		screenDimension.y = Screen.height;
		screenDimension.z = 0;
		screenDimension = Camera.main.ScreenToWorldPoint(screenDimension);


		screenDimension.x = GenCoord(- screenDimension.x + 5, screenDimension.x - 5);
		screenDimension.y = GenCoord(- screenDimension.y + 5, screenDimension.y - 5);
		screenDimension.z = 0;

		return screenDimension;
	
	}

	void SpawnPrefabWithPosition(GameObject prefab, Vector3 cposition){

		GameObject objet;
		UnityEngine.UI.Image objetImage;
		RectTransform rtransform;

		objet = Instantiate(prefab) as GameObject;
		objetImage = objet.GetComponentInChildren<Image>();
		rtransform = objetImage.GetComponent<RectTransform>();

		rtransform.transform.position += cposition*10;
	}

	float GenCoord(float min, float max){
		float new_coord;

		new_coord = Random.Range(min - 10, max + 10);

		return new_coord;
	}

	public void ValidKills(){
		if (killedCats == numberOfCats){
			manager.Gagne();
		} else {
			manager.Perdu();
		}
	}

	public void Meow() {
		AudioSource player;

		player = this.gameObject.GetComponent<AudioSource>();
		player.clip = Meows[(int) Random.Range(0, Meows.Length)];
		player.Play();
	}
}
