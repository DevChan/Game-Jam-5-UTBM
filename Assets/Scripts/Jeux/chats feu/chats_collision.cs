using UnityEngine;
using System.Collections;

public class chats_collision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		CheckCollision();
	}

	void CheckCollision(){
		GameObject scripts;

		if(areColliding(this.gameObject, GameObject.FindWithTag("Feu"), 30)) {
			Destroy(this.gameObject);
			scripts = GameObject.FindWithTag("SceneScripts");
			scripts.gameObject.GetComponent<chats>().killedCats += 1;

			scripts.gameObject.GetComponent<chats>().Meow();
		}
	}
	
	private bool areColliding (GameObject Object1,GameObject Object2,float precision){
		float Proximity;

		Proximity = (Mathf.Sqrt(Mathf.Pow(Object1.transform.position.x-Object2.transform.position.x,2)+Mathf.Pow(Object1.transform.position.y-Object2.transform.position.y,2)));
		return (Proximity < precision);
	}
}
