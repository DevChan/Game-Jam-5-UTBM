﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PutItOnManager : MonoBehaviour {
	private GameManager Manager;
	public Sprite[] Images;
	public Image Image1;
	public Image Image2;
	public Text Order;
	private float Proximity;
	private Vector3 fixedObject;
	// Use this for initialization
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();
		Image1.sprite = Images[(int)(Random.Range (0, Images.Length))];
		do {
			Image2.sprite = Images[(int)(Random.Range (0, Images.Length))];
		} while (Image2.sprite == Image1.sprite);
		Order.text = "Put " + Image1.sprite.name + " on " + Image2.sprite.name;
		if (Random.Range (0, 2)==1) {
			fixedObject = Image1.gameObject.transform.position;
			Image1.gameObject.transform.position = Image2.gameObject.transform.position;
			Image2.gameObject.transform.position = fixedObject;
		}
		fixedObject = Image2.gameObject.transform.position;
	}

	// Update is called once per frame
	void Update () {
		//Debug.Log(Mathf.Sqrt(Mathf.Pow(Image1.gameObject.transform.position.x,2)+Mathf.Pow(Image2.gameObject.transform.position.x,2))+" "+Mathf.Sqrt(Mathf.Pow(Image1.gameObject.transform.position.y,2)+Mathf.Pow(Image2.gameObject.transform.position.y,2)));
		//Proximity = (Mathf.Sqrt(Mathf.Pow(Image1.gameObject.transform.position.x-Image2.gameObject.transform.position.x,2)+Mathf.Pow(Image1.gameObject.transform.position.y-Image2.gameObject.transform.position.y,2)));
		if (Manager.isEnded) {
			Manager.Perdu();
		}
		if (areColliding(Image1.gameObject,Image2.gameObject,30)) {
			if (fixedObject == Image2.gameObject.transform.position) {
				Manager.Gagne ();
			} else {
				Manager.Perdu ();
			}

		}
	}

	private bool areColliding (GameObject Object1,GameObject Object2,float precision){
		Proximity = (Mathf.Sqrt(Mathf.Pow(Object1.transform.position.x-Object2.transform.position.x,2)+Mathf.Pow(Object1.transform.position.y-Object2.transform.position.y,2)));
		return (Proximity < precision);
	}
}
