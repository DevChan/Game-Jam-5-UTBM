﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoToScript : MonoBehaviour {
	public GameObject Joueur;
	public GameObject Cible;
	private GameManager Manager;
	public Sprite[] Images;
	public Text Order;
	private Rigidbody2D RBjoueur;
	public Vector3 Mouvement;
	public GameObject obstacle;
	// Use this for initialization
	void Start () {
		RBjoueur = Joueur.GetComponent<Rigidbody2D> ();
		Manager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();

		Cible.GetComponent<SpriteRenderer>().sprite = Images[(int)(Random.Range (0, Images.Length))];
		Cible.AddComponent<PolygonCollider2D> ();
		Order.text = "Go to "+Cible.GetComponent<SpriteRenderer>().sprite.name + " (using arrows)";

		do {
			obstacle.GetComponent<SpriteRenderer>().sprite = Images[(int)(Random.Range (0, Images.Length))];
		} while (obstacle.GetComponent<SpriteRenderer>().sprite == Cible.GetComponent<SpriteRenderer>().sprite);
		if (Random.Range (0, 2) == 1) {
			obstacle.AddComponent<PolygonCollider2D> ();
		} else {
			obstacle.SetActive (false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Manager.isEnded) {
			Manager.Perdu ();
		}
		Mouvement = new Vector3(Input.GetAxis("Horizontal")*10,Input.GetAxis("Vertical")*10);
		if (Joueur.GetComponent<PolygonCollider2D> ().IsTouching(Cible.GetComponent<PolygonCollider2D>())) {
			Manager.Gagne();
		}
		if (Joueur.GetComponent<PolygonCollider2D> ().IsTouching(obstacle.GetComponent<PolygonCollider2D>())) {
			Manager.Perdu();
		}
	}
	void FixedUpdate () {
		RBjoueur.velocity = Mouvement;
	}
}
