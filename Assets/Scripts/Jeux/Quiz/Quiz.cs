using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Quiz : MonoBehaviour {

	public char goodAnswer;

	private List<Question> quizQuestions = new List<Question>();
	private int choosenQuestion;
	private GameManager manager;

	void FeedQuiz(){
		quizQuestions.Add(new Question("Rekt or Not Rekt ?", new List<string>(new string[] {"Rekt", "Tyranosurus Rekt"})));
		quizQuestions.Add(new Question("Do you like Trump ?", new List<string>(new string[] {"Obviously", "Never Ever"})));
		quizQuestions.Add(new Question("Are you a faggot ?", new List<string>(new string[] {"I'm a faggot !", "Why ?"})));
		quizQuestions.Add(new Question("Ho do you like cats ?", new List<string>(new string[] {"They are so cute !", "Only well prepared"})));
		quizQuestions.Add(new Question("1+1 = ?", new List<string>(new string[] {"20", "02"})));
		quizQuestions.Add(new Question("What's red ?", new List<string>(new string[] {"A color", "Your mom"})));
		quizQuestions.Add(new Question("What's the best OS ?", new List<string>(new string[] {"Windows", "Internet Explorer"})));
		quizQuestions.Add(new Question("PC or Console ?", new List<string>(new string[] {"Console", "MAC"})));
		quizQuestions.Add(new Question("53645x463433 = ?", new List<string>(new string[] {"1", "Screw you"})));
		quizQuestions.Add(new Question("Reddit or 4chan ?", new List<string>(new string[] {"Fb ofc", "Google+"})));
		quizQuestions.Add(new Question("678905432/678905432 = ?", new List<string>(new string[] {"1", "Screw you ?"})));
		quizQuestions.Add(new Question("Trick or treat ?", new List<string>(new string[] {"Trit", "Treack"})));
		quizQuestions.Add(new Question("A or B ?", new List<string>(new string[] {"B", "A"})));
		quizQuestions.Add(new Question("A and B ?", new List<string>(new string[] {"Yes", "No"})));
		quizQuestions.Add(new Question("How are you ?", new List<string>(new string[] {"Fine", "Fine"})));
		quizQuestions.Add(new Question("A and notB ?", new List<string>(new string[] {"C", "B and notA"})));
		quizQuestions.Add(new Question("Are you nazi ?", new List<string>(new string[] {"Yes", "Maybe"})));
		quizQuestions.Add(new Question("Who are you ?", new List<string>(new string[] {"A pedo", "A nazi"})));
		quizQuestions.Add(new Question("cos(x+pi/2) ?", new List<string>(new string[] {"sin(a)", "0.4343"})));
		quizQuestions.Add(new Question("What is Shrek ?", new List<string>(new string[] {"Love", "Life"})));
		quizQuestions.Add(new Question("Coca or pepsi ?", new List<string>(new string[] {"Life", "Green"})));
		quizQuestions.Add(new Question("How is Jon ?", new List<string>(new string[] {"Snow", "Dead"})));
		quizQuestions.Add(new Question("Fuck you ?", new List<string>(new string[] {"W/pleasure", "Fuck you !"})));
	}

	// Use this for initialization
	void Start () {
		Text newOrder;

		FeedQuiz();

		manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
		newOrder = GameObject.FindGameObjectWithTag("Order").GetComponent<Text>();

		goodAnswer = AorB();

		newOrder.text = "Answer the question with " + goodAnswer + " !";
		choosenQuestion = SelectQuestion();

		UpdateUI();
	}
	
	// Update is called once per frame
	void Update () {
		if (manager.isEnded){
			manager.Perdu();
		}
	}

	int SelectQuestion(){
		return (int) (Random.Range(0, quizQuestions.Count));
	}

	char AorB(){
		int i;

		i = Random.Range(0,2);
		if (i == 0){
			return 'A';
		} else {
			return 'B';
		}
	}

	void UpdateUI(){
		GameObject quiz;	
		UnityEngine.UI.Text text;

		quiz = GameObject.Find("QuestionForm");
		text = quiz.GetComponentInChildren<Text>();
		text.text = quizQuestions[choosenQuestion].title;

		quiz = GameObject.Find("AnswerA");
		text = quiz.gameObject.GetComponentInChildren<Text>();
		text.text = quizQuestions[choosenQuestion].list[0];

		quiz = GameObject.Find("AnswerB");
		text = quiz.gameObject.GetComponentInChildren<Text>();
		text.text = quizQuestions[choosenQuestion].list[1];

	}

	public void ChooseA(){
		if(goodAnswer == 'A'){
			manager.Gagne();
		} else {
			manager.Perdu();
		}
	}

	public void ChooseB(){
		if(goodAnswer == 'B'){
			manager.Gagne();
		} else {
			manager.Perdu();
		}
	}
}
