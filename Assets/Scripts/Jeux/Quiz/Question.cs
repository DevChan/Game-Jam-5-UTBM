﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Question {

	public List<string> list = new List<string>();
	public string title;
	
	public Question(string _title, List<string> _list)
	{
		list = _list;
		title = _title;
	}
	
}

