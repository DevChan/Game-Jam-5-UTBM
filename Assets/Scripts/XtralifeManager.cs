﻿using UnityEngine;
using System.Collections;
using CotcSdk;
using UnityEngine.UI;
using System;

#if UNITY_5_4_OR_NEWER
using UnityEngine.Networking;
#else
using UnityEngine.Experimental.Networking;
#endif

public class XtralifeManager : MonoBehaviour {
	// The cloud allows to make generic operations (non user related)
	private Cloud Cloud;
	// The gamer is the base to perform most operations. A gamer object is obtained after successfully signing in.
	private Gamer Gamer;
	// When a gamer is logged in, the loop is launched for domain private. Only one is run at once.
	private DomainEventLoop Loop;

	public const string GAMERID_KEY = "GamerId";
	public const string GAMERSECRET_KEY = "GamerSecret";
	public const string BESTSCORE_KEY = "BestScore";

	void Start() {
		// Link with the CotC Game Object
		var cb = FindObjectOfType<CotcGameObject>();
		if (cb == null) {
			Debug.LogError("Please put a Clan of the Cloud prefab in your scene!");
			return;
		}
		// Log unhandled exceptions (.Done block without .Catch -- not called if there is any .Then)
		Promise.UnhandledException += (object sender, ExceptionEventArgs e) => {
			Debug.LogError("Unhandled exception: " + e.Exception.ToString());
		};
		// Initiate getting the main Cloud object
		cb.GetCloud().Done(cloud => {
			Cloud = cloud;
			// Retry failed HTTP requests once
			Cloud.HttpRequestFailedHandler = (HttpRequestFailedEventArgs e) => {
				if (e.UserData == null) {
					e.UserData = new object();
					e.RetryIn(1000);
				}
				else
					e.Abort();
			};
			Debug.Log("Setup done");
		});
		DoLogin ();
	}

	// Signs in with an anonymous account or with the previously used one
	public void DoLogin() { 
		// Call the API method which returns an Promise<Gamer> (promising a Gamer result).
		// It may fail, in which case the .Then or .Done handlers are not called, so you
		// should provide a .Catch handler.
		if (isGamerSaved()) {
			//Get the gamerdata
			ResumeSession();
		} else {
			newLogin ();
		}
	}

	// Update is called once per frame
	void Update () {
	
	}


	// Invoked when any sign in operation has completed
	private void DidLogin(Gamer newGamer) {
		if (Gamer != null) {
			Debug.LogWarning("Current gamer " + Gamer.GamerId + " has been dismissed");
			Loop.Stop();
		}
		Gamer = newGamer;
		Loop = Gamer.StartEventLoop();
		Loop.ReceivedEvent += Loop_ReceivedEvent;
	//	Debug.Log("Signed in successfully (ID = " + Gamer.GamerId + ")");
		SaveGamer ();
		var gm = FindObjectOfType<GameManager>();
		gm.playerConnected = true;
	}

	private void Loop_ReceivedEvent(DomainEventLoop sender, EventLoopArgs e) {
		Debug.Log("Received event of type " + e.Message.Type + ": " + e.Message.ToJson());
	}

	public Gamer getGamer(){
		return Gamer;
	}

 	private void ResumeSession(){
        var cotc = FindObjectOfType<CotcGameObject>();

        cotc.GetCloud().Done(cloud => {
            Cloud.ResumeSession(
				gamerId: PlayerPrefs.GetString(GAMERID_KEY),
				gamerSecret: PlayerPrefs.GetString(GAMERSECRET_KEY))
				.Then (gamer => DidLogin (gamer))
				.Catch (ex => {
					// The exception should always be CotcException
					CotcException error = (CotcException)ex;
					Debug.LogError ("Failed to login: " + error.ErrorCode + " (" + error.HttpStatusCode + ")");
				});
        });  
    }

	public void newLogin(){
		Cloud.LoginAnonymously ()
			.Then (gamer => DidLogin (gamer))
			.Catch (ex => {
				// The exception should always be CotcException
				CotcException error = (CotcException)ex;
				Debug.LogError ("Failed to login: " + error.ErrorCode + " (" + error.HttpStatusCode + ")");
			});
	}
	public void SaveGamer(){
		//if (!isGamerSaved ()) {
			PlayerPrefs.SetString (GAMERID_KEY, Gamer.GamerId);
			PlayerPrefs.SetString (GAMERSECRET_KEY, Gamer.GamerSecret);
		//}
	}
	public bool isGamerSaved(){
		return PlayerPrefs.HasKey (GAMERID_KEY) && PlayerPrefs.HasKey (GAMERSECRET_KEY);
	}

	public void PostScore(int score)
    {
        // currentGamer is an object retrieved after one of the different Login functions.

		Gamer.Scores.Domain("private").Post(score, "intermediateMode", ScoreOrder.HighToLow,
        "context for score", false)
        .Done(postScoreRes => {
            Debug.Log("Post score: " + postScoreRes.ToString());
        }, ex => {
            // The exception should always be CotcException
            CotcException error = (CotcException)ex;
            Debug.LogError("Could not post score: " + error.ErrorCode + " (" + error.ErrorInformation + ")");
        });
    }

	public void GetRank(int score,Text board)
    {
		//Gamer.Scores.Domain("private").GetRank(PlayerPrefs.GetInt(BESTSCORE_KEY), "intermediateMode")
		Gamer.Scores.Domain("private").GetRank(score, "intermediateMode")
		.Done(getRankRes => {
            Debug.Log("Rank for score: " + getRankRes);
				if (board.text == ""){
					board.text = "Your rank :" + getRankRes;
				}else{
					board.text = "Your rank :" + getRankRes + board.text;
				}
			}, ex => {
            // The exception should always be CotcException
            CotcException error = (CotcException)ex;
            Debug.LogError("Could not get rank for score: " + error.ErrorCode + " (" + error.ErrorInformation + ")");
        });
    }


	public void CenteredScores(Text board)
    {
        // currentGamer is an object retrieved after one of the different Login functions.
        Gamer.Scores.Domain("private").PagedCenteredScore("intermediateMode", 3)
        .Done(centeredScoresRes => {
				foreach(var score in centeredScoresRes){
					board.text = board.text + "\n" + score.Rank+ ". " + score.GamerInfo["gamer_id"].AsString().Substring(0,7) + ": " + score.Value;
					Debug.Log(score.Rank + ". " + score.GamerInfo["profile"]["displayName"] + ": " + score.Value);
				}
        }, ex => {
            // The exception should always be CotcException
            CotcException error = (CotcException)ex;
            Debug.LogError("Could not get centered scores: " + error.ErrorCode + " (" + error.ErrorInformation + ")");
        });
		
    }
    
}
