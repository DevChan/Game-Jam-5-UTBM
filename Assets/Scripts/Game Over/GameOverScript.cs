﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverScript : MonoBehaviour {
	public Text Score;
	public Text Ranks;
	private GameManager Manager;
	public GameObject Explosion;
	public Image explosionImage;

	// Use this for initialization
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();

		if (Manager.Score == 0) {
			Score.text = "You must obey to the red text";
			Ranks.text = "";
		} else {
			
			bool best = Manager.sendScore ();
			if (best) {
				Score.text = "New best score : " + Manager.Score + "!";
			} else {
				Score.text = "Score : " + Manager.Score;
			}
			try{
				Manager.SetScoreboard (Ranks);
			}catch{
				Ranks.text = "Leaderboards error, reload the game";
			}
		}

	}

	public void Continue(){
		Manager.Recommencer ();
	}
	public void Quit(){
		Manager.Exit ();
	}
	// Update is called once per frame
	void Update () {
		explosionImage.sprite = Explosion.GetComponent<SpriteRenderer>().sprite;
		if (Manager.isEnded) {
			Manager.Recommencer ();
		}
	
	}
	public void reloadBoard(){
		try{
			Ranks.text = "";
			Manager.SetScoreboard (Ranks);
		}catch{
			Ranks.text = "Leaderboards error, reload the game";
		}
	}
}
