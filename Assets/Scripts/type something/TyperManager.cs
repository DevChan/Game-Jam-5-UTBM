﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class TyperManager : MonoBehaviour {
	public string ToType="default";
	public string[] Words;
	private int i=0;
	private GameManager Manager;
	public Text toTypeUI;
	public Text Typing;
	// Use this for initialization
	void Start () {
		Manager = GameObject.FindGameObjectWithTag ("GameManager").GetComponent<GameManager>();
		ToType = Words [(int)(Random.Range (0, Words.Length))];
		toTypeUI.text = ToType;
	}

	// Update is called once per frame
	void Update () {
		if (Manager.isEnded) {
			Perdu ();
		}
		if (i > 0) {
			Typing.text = ToType.Substring(0,i);
		}
		foreach (char c in Input.inputString) {
			if (i < ToType.Length) {
				if (ToType [i] == c) {
					i++;
					if (i == ToType.Length) {
						Manager.Gagne ();
					}
				} else {
					i = 0;
					Typing.text = "_ fail...try again _";
				}

			} 
		}
	}
	public void Perdu(){
		Manager.Perdu ();
	}
}
